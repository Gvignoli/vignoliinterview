/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms3_exam;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author guy
 */
public class ParseCSV {
    
    public static int successNum = 0;
    public static int recordsReceived = 0;
    public static int failNumber = 0;

    public static List<List<String>> parseCSV(String fileName) throws IOException{
            String line = null;
    BufferedReader stream = null;
    //creating array list to hold rows of csv
    List<List<String>> csvData = new ArrayList<List<String>>();
    boolean firstLine = true;
    try {
        stream = new BufferedReader(new FileReader(fileName));
        
        while ((line = stream.readLine()) != null){
            recordsReceived++;
            if(firstLine){ //this just skips the first line of the file beacuse it holds column name
                firstLine = false;
                continue;
            } 

            else if(line != null){
                line = line.replaceAll("\\'", "\\\\'");//escaping single quote from elements to make them mysql friendly
                // regex to find commas that are surrounded by double quotes and ignore them 
               String[] currLine = line.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");//splitting the lines and storing in a variable
                //check for incomplete rows and handle it
               if(currLine.length !=10){// found a malformed line send it to bad-data.csv
                BadDataWriter.writeToCsvFile(currLine);
                failNumber++;
                }
               else{
                //adding rows to large csv array split on comma
                csvData.add(Arrays.asList(currLine));
                successNum++;
               }
            }
        }

    } finally {
        if (stream != null)
            stream.close();
    }
    return csvData;
    }

    
}
