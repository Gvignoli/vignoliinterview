/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms3_exam;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author guy
 */
public class DBWriter {
    
    
    public static void writeToDB(List<List<String>> csvData){
        //using try-with-resources
        try (Connection con = DBConnect.getConnection();
        Statement stmt = con.createStatement();)
        {
            System.out.println("Writing entries to DataBase");
            for(int i =0; i<csvData.size(); i++){//looping through rows 

           //I know this next part looks terrible
            //row remains the same through each iteration of accessing differe "column from arrarylist of type list
            String query = "insert into x (A,B,C,D,E,F,G,H,I,J) values ('" +csvData.get(i).get(0)+"','"+csvData.get(i).get(1)+"','"+csvData.get(i).get(2)+"','"+csvData.get(i).get(3)+"','"+
                    csvData.get(i).get(4)+"','"+csvData.get(i).get(5)+ "','"+csvData.get(i).get(6)+"','"+csvData.get(i).get(7)+"','"+csvData.get(i).get(8)+"','"+csvData.get(i).get(9)+"')";
            
                stmt.executeUpdate(query);//executing the formed statment above
            }
        } catch (SQLException e) {
            e.printStackTrace();
                System.out.println("SQLException: " + e.getMessage());
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("VendorError: " + e.getErrorCode());
        }
    
    }
}
