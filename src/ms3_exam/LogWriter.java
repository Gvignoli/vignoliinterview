/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms3_exam;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author guy
 * Class to write the final log file
 */
public class LogWriter {
    
    public static void writeLog(){
         BufferedWriter writer = null;
        try {
            //create a file to write log to
            File logFile = new File("statLog.txt");

            writer = new BufferedWriter(new FileWriter(logFile));
            //getting variables from parseCSV class and writing them to file
            writer.write("Records Received: " + ParseCSV.recordsReceived + "\n");
            writer.write("Records Successful: "+ ParseCSV.successNum + "\n");
            writer.write("Records Failed: "+ ParseCSV.failNumber + "\n");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer 
                writer.close();
            } catch (Exception e) {
            }
        }
    }
    
}
