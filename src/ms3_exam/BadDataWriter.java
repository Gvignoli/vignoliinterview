/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms3_exam;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author guy
 * writes all bad data to a csv file
 * data comes in as a string array
 */
public class BadDataWriter {
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static FileWriter fileWriter = null;
        
        private static Date timeStamp = new Timestamp(Calendar.getInstance().getTime().getTime());//getting current timestamp
        private static String fileName = new SimpleDateFormat("'bad-data'yyyyMMddhhmm'.csv'").format(new Date());//filename with timestamp
        private static File dataFile = new File(fileName);//creating file with name
	//CSV file header
	private static final String FILE_HEADER = "A,B,C,D,E,F,G,H,I,J";//wanted to include column names but couldnt get logic correct to only occur once


        
        public static void writeToCsvFile(String[] badLine) {
		
            try {
                    fileWriter = new FileWriter(dataFile, true);

                    fileWriter.append(Arrays.toString(badLine));//writing line

                    //Add a new line separator after the header
                    fileWriter.append(NEW_LINE_SEPARATOR);
			
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
			}
			
		}
	} 
}
