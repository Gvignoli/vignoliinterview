
package ms3_exam;

/**
 *
 * @author guy
 */


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
 
public class DBConnect {
 //clas that creates database connection based on db.properties file. 
    
    public static Connection getConnection() {
        Properties props = new Properties();
        Connection con = null;
        try {
            // mysql port: 3306
            //mysql root pass: toor
            // load database properties file
            props.load(DBConnect.class.getResourceAsStream("db.properties"));
            Class.forName(props.getProperty("DB_DRIVER_CLASS"));
 
            // create the connection now
            con = DriverManager.getConnection(props.getProperty("DB_URL"),
                    props.getProperty("DB_USERNAME"),
                    props.getProperty("DB_PASSWORD"));
            System.out.println("Successfully connected to database");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error Connecting to database");
        }
        return con;
    }
}

